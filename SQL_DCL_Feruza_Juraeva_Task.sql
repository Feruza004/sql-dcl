-- task 1
CREATE ROLE rentaluser LOGIN PASSWORD 'rentalpassword';
GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

-- task 2
GRANT SELECT ON TABLE customer TO rentaluser;
select * from customer;

-- task 3
CREATE ROLE rental;
GRANT rental TO rentaluser;

-- task 4
GRANT INSERT ON TABLE rental TO rental;
GRANT UPDATE ON TABLE rental TO rental;

INSERT INTO rental ( rental_date, inventory_id, customer_id, staff_id, return_date)
VALUES ('2023-11-26 14:30:00', 1711,333, 2, '2023-11-27 12:00:00');

UPDATE rental SET return_date = '2023-11-28 11:00:00' WHERE customer_id = 16050;

--task 5
REVOKE INSERT ON rental FROM rental;
--error appears when insert is executed after revoke (only if DB connected with rentaluser login role)
INSERT INTO rental ( rental_date, inventory_id, customer_id, staff_id, return_date)
VALUES ('2023-11-26 14:30:00', 1525,408, 2, '2023-11-28 12:00:00');


-- task 6
-- GRANT does not support row lever access so, to make client_mary_smith be able to see
-- only her data in rental and payment tables, views are used 

CREATE ROLE client_mary_smith LOGIN PASSWORD '123';

GRANT CONNECT ON DATABASE dvdrental TO client_mary_smith;


CREATE VIEW view_payment_for_mary AS 
SELECT * FROM payment WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Mary' AND last_name = 'Smith');

GRANT SELECT ON view_payment_for_mary TO client_mary_smith;

CREATE VIEW view_rental_for_mary AS 
SELECT * FROM payment WHERE customer_id = (SELECT customer_id FROM customer WHERE first_name = 'Mary' AND last_name = 'Smith');

GRANT SELECT ON view_rental_for_mary TO client_mary_smith;

SELECT current_user ;
SELECT * FROM view_rental_for_mary;
SELECT * FROM view_payment_for_mary;
